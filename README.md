# SemVer [![build status](https://framagit.org/Bat/semver/badges/master/build.svg)](https://framagit.org/Bat/semver/commits/master)

SemVer is a little library that helps you manipulating [semantic versions](http://semver.org).

# Example usage

```vala
using SemVer;

void main () {
    var v1 = new SemVer.Version (0, 2, 3);
    var v2 = new SemVer.Version.parse ("0.3.6");
    if (v1.lt (v2)) { // true
        print (@"$v1 is less recent than $v2 \n");
    }

    var rng = new Range ("0.1.0 - 0.5.0");
    if (v1 in rng) { // true
        print ("$v1 in $rng \n");
    }

    rng = new Range ("0.1.* || ^0.3.1");
    if (v2 in rng) { // true
        print ("$v1 in $rng \n");
    }

    v1.publish (VersionType.MAJOR); // v1 = 1.0.0
    v1.publish (VersionType.PATCH); // v1 = 1.0.1
    v1.publish (VersionType.MINOR); // v1 = 1.1.0
}
```
