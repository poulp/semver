namespace SemVer {

    /**
    * Checks if a string represents a semantic version.
    *
    * @param raw_version The {@link GLib.string} to check.
    * @return true if the string represents a semantic version, false otherwise.
    */
    public bool valid (string raw_version) {
        Regex validator = /^\d+\.\d+\.\d+$/;
        return validator.match (raw_version);
    }

    /**
    * Cleans a raw version.
    *
    * It removes leading and trailing whitespaces and trailing v and =.
    *
    * @param dirty_version The version to clean.
    * @return The version, cleaned
    */
    public string clean (string dirty_version) {
        Regex dirty_finder = /^[=v]*/;
        return dirty_finder.replace (dirty_version.strip (), dirty_version.strip ().length, 0, "");
    }
}
