namespace SemVer {

    /**
    * A class representing a range of versions
    *
    * Exemples of ranges.
    *
    * {{{
    *   >1.0.0
    *   >1.0.0 <=2.0.0
    *   0.2.0 - 0.3.0
    *   ~2.0
    *   ^0.3.1
    *   *
    *   0.*
    *   0.1.* || >0.5.0
    * }}}
    */
    public class Range : Object {

        /**
        * The raw version of this range.
        */
        public string raw { get; private set; }

        /**
        * A raw version of this string, but simplified.
        *
        * Special ranges (X.Y.Z - A.B.C, ~2.0, ^0.2.1, 0.*) are simplified
        * into comparison operators.
        *
        * For instance, //1.2.3 - 2.3.4// becomes //>=1.2.3 <=2.3.4//.
        */
        public string basic { get; private set; }

        /**
        * Parse and simplifies a range.
        *
        * @param expr The raw {@link string} representing the range to parse.
        */
        public Range (string expr) {
            this.raw = expr;

            string[] simplified = new string[] {};
            foreach (string subrange in expr.split ("||")) { // for each part of the range
                subrange = subrange.strip ();

                // hyphen range
                if ("-" in subrange) {
                    string v1 = subrange.split ("-")[0].strip ();
                    string v2 = subrange.split ("-")[1].strip ();
                    while (!valid (v1)) {
                        v1 += ".0";
                    }

                    if (!valid (v2)) {
                        string[] parts = v2.split (".");
                        parts[parts.length - 1] = (int.parse (parts[parts.length - 1]) + 1).to_string ();
                        v2 = string.joinv (".", parts);
                    }
                    while (!valid (v2)) {
                        v2 += ".0";
                    }

                    simplified += ">=%s <=%s".printf (v1, v2);
                } else if (subrange.has_prefix ("~")) { // tilde range
                    string min = subrange[1:subrange.length];
                    while (!valid (min)) {
                        min += ".0";
                    }

                    string[] parts = subrange[1:subrange.length].split (".");
                    parts[parts.length - 1] = (int.parse (parts[parts.length - 1]) + 1).to_string ();
                    string max = string.joinv (".", parts);
                    while (!valid (max)) {
                        max += ".0";
                    }
                    simplified += ">=%s <%s".printf (min, max);
                } else if (subrange.has_prefix ("^")) { // carret range
                    subrange = subrange.replace ("x", "0");
                    while (!valid (subrange[1:subrange.length])) {
                        subrange += ".0";
                    }

                    string min = subrange[1:subrange.length];

                    int last_left_zero = -1;
                    string[] parts = min.split (".");
                    int i = 0;
                    foreach (string part in parts) {
                        if (part == "0") {
                            last_left_zero = i + 1;
                        } else {
                            break;
                        }
                        i++;
                    }
                    parts[last_left_zero] = (int.parse (parts[last_left_zero]) + 1).to_string ();
                    for (int j = last_left_zero + 1; j < parts.length; j++) {
                        parts[j] = "0";
                    }

                    simplified += ">=%s <%s".printf (min, string.joinv (".", parts));
                } else if ("*" in subrange || "x" in subrange || "X" in subrange) { // wildcard range
                    subrange = subrange.replace ("x", "*").replace ("X", "*");
                    
                    if (subrange == "*" || subrange == "*.*" || subrange == "*.*.*") {
                        simplified += ">=0.0.0";
                    } else {
                        string min = subrange.replace ("*", "0");
                        while (!valid (min)) {
                            min += ".0";
                        }

                        string[] parts = subrange.split (".");
                        int first_x_index = 1;
                        int i = 0;
                        foreach (string part in parts) {
                            if (part == "*") {
                                first_x_index = i;
                                break;
                            }
                            i++;
                        }
                        parts[first_x_index - 1] = (int.parse (parts[first_x_index - 1]) + 1).to_string ();

                        int j = 0;
                        foreach (string part in parts) {
                            parts[j] = part == "*" ? "0" : part;
                            j++;
                        }
                        simplified += ">=%s <%s".printf (min, string.joinv (".", parts));
                    }
                } else {
                    simplified += subrange; // range is already "simplified"
                }
            }
            this.basic = string.joinv (" || ", simplified);
        }

        /**
        * Creates a new hyphen range.
        *
        * @param min The minimum version.
        * @param max The maximum version.
        */
        public Range.hyphen (Version min, Version max) {
            this (@"$min - $max");
        }

        /**
        * Creates a new alternative range (||) beetwen two other range.
        *
        * @param r1 The first possible range.
        * @param r2 The second possible range.
        */
        public Range.or (Range r1, Range r2) {
            this (@"$r1 || $r2");
        }

        /**
        * Checks if a version matches a range.
        *
        * You can use the //in// keyword to invoke this method.
        *
        * {{{
        *   // This two conditions are equivalent
        *   // But the first one is nicer
        *   if (version in range) {
        *       foo ();
        *   }
        *
        *   if (range.contains (version)) {
        *       bar ();
        *   }
        * }}}
        *
        * @param v The {@link SemVer.Version} to check.
        * @return true if the version is in the range, false otherwise.
        */
        public bool contains (Version v) {
            bool res = false;
            if ("||" in this.basic) {
                foreach (string child in this.basic.split ("||")) {
                    res = res || new Range (child.strip ()).contains (v);
                }
            } else { // Atomic range
                res = true;
                foreach (string condition in this.basic.split (" ")) {
                    if (condition == "") {
                        continue;
                    }

                    if (valid (condition)) {
                        res = res && v.eq (new Version.parse (condition));
                    } else if (condition.has_prefix ("<=")) {
                        res = res && v.lte (new Version.parse (condition[2:condition.length]));
                    } else if (condition.has_prefix ("<")) {
                        res = res && v.lt (new Version.parse (condition[1:condition.length]));
                    } else if (condition.has_prefix (">=")) {
                        res = res && v.gte (new Version.parse (condition[2:condition.length]));
                    } else if (condition.has_prefix (">")) {
                        res = res && v.gt (new Version.parse (condition[1:condition.length]));
                    }
                }
            }
            return res;
        }

        /**
        * Gives a {@link string} representation of this.
        *
        * @return This range, as a {@link string}.
        */
        public string to_string () {
            return this.raw;
        }
    }
}
