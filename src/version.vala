namespace SemVer {

    /**
    * Contains the different types of versions (major, minor and patch).
    */
    public enum VersionType {
        MAJOR,
        MINOR,
        PATCH;

        /**
        * Gives a {@link GLib.string} representation of this.
        *
        * @return The string representation of this.
        */
        public string to_string () {
            switch (this) {
                case VersionType.MAJOR:
                    return "Major";
                case VersionType.MINOR:
                    return "Minor";
                default:
                    return "Patch";
            }
        }
    }

    /**
    * Representation of a version, following the semantic versionning guidelines.
    */
    public class Version : Object {

        /**
        * The major version.
        */
        public int major { get; private set; default = 0; }

        /**
        * The minor version.
        */
        public int minor { get; private set; default = 1; }

        /**
        * The patch version
        */
        public int patch { get; private set; default = 0; }

        /**
        * Creates a new {@link SemVer.Version} instance.
        *
        * @param major The major version.
        * @param minor The minor version.
        * @param patch The patch version.
        */
        public Version (int major = 0, int minor = 1, int patch = 0) {
            this.major = major;
            this.minor = minor;
            this.patch = patch;
        }

        /**
        * Creates a new {@link SemVer.Version} from a {@link string}
        *
        * Be sure that the version you want to parse is valid, with {@link SemVer.valid}.
        *
        * @param raw The version to parse
        */
        public Version.parse (string raw) requires (valid (raw)) {
            string[] parts = raw.split (".");
            this.major = int.parse (parts[0]);
            this.minor = int.parse (parts[1]);
            this.patch = int.parse (parts[2]);
        }

        /**
        * Increments the version number.
        *
        * = Example =
        *
        * {{{
        *   Version v = new Version ();    // v is 0.1.0
        *   v.publish (VersionType.MINOR); // v is now 0.2.0
        *   v.publish (VersionType.PATCH); // v is now 0.2.1
        *   v.publish (VersionType.MAJOR); // v is now 1.0.0
        *   v.publish (VersionType.MINOR); // And now, it's 2.1.0
        * }}}
        *
        * @param version_type The part of the version to update.
        */
        public void publish (VersionType version_type) {
            switch (version_type) {
                case VersionType.MAJOR:
                    this.major++;
                    this.minor = 0;
                    this.patch = 0;
                    break;
                case VersionType.MINOR:
                    this.minor++;
                    this.patch = 0;
                    break;
                case VersionType.PATCH:
                default:
                    this.patch++;
                    break;
            }
        }

        public bool satisfies (string expr) {
            Range rng = new Range (expr);
            return this in rng;
        }

        /**
        * Returns a string representation of this.
        *
        * It looks like major.minor.patch
        *
        * @return The string representation of this version.
        */
        public string to_string () {
            return "%d.%d.%d".printf (this.major, this.minor, this.patch);
        }

        /**
        * Check if two versions are equals.
        *
        * @param other The version that will be compared to this.
        * @return true if the versions are equals, false otherwise.
        */
        public bool eq (Version other) {
            return other.major == this.major && other.minor == this.minor && other.patch == this.patch;
        }

        /**
        * Check if two versions are differents.
        *
        * @param other The version that will be compared to this.
        * @return true if the versions are differents, false otherwise.
        */
        public bool neq (Version other) {
            return !this.eq (other);
        }

        /**
        * Checks that this version is less than an other one.
        *
        * @param other The other version.
        * @return true if this is less than the other, false otherwise.
        */
        public bool lt (Version other) {
            return  (other.major > this.major) ||
                    (other.major == this.major && other.minor > this.minor) ||
                    (other.major == this.major && other.minor == this.minor && other.patch > this.patch);
        }

        /**
        * Checks that this version is greater than an other one.
        *
        * @param other The other version.
        * @return true if this is greater than the other, false otherwise.
        */
        public bool gt (Version other) {
            return  (other.major < this.major) ||
                    (other.major == this.major && other.minor < this.minor) ||
                    (other.major == this.major && other.minor == this.minor && other.patch < this.patch);
        }

        /**
        * Checks that this version is less or equal than an other one.
        *
        * @param other The other version.
        * @return true if this is less or equal than the other, false otherwise.
        */
        public bool lte (Version other) {
            return !this.gt (other);
        }

        /**
        * Checks that this version is greater or equal than an other one.
        *
        * @param other The other version.
        * @return true if this is greater or equal than the other, false otherwise.
        */
        public bool gte (Version other) {
            return !this.lt (other);
        }
    }
}
