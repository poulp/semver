cat > build/semver.pc <<EOF
includedir=/usr/include/
libdir=/usr/lib64

Name: semver
Description: Semantic versionning
Version: 1.0.3
CFlags: -I\${includedir}semver
Libs: -L\${libdir} -l semver
EOF
