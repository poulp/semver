using SemVer;

/**
* A little programm to test SemVer
*/
int main (string[] args) {
    Test.init (ref args);

    Test.add_func ("/semver/valid", () => {
        if (valid ("beta 0.3.0")) {
            Test.fail ();
        }

        if (!valid ("12.4.5")) {
            Test.fail ();
        }
    });

    Test.add_func ("/semver/clean", () => {
        string expected = "0.4.5";
        string result = clean ("  =v0.4.5  ");
        if (result != expected) {
            Test.fail ();
        }
    });

    Test.add_func ("/semver/version/parse", () => {
        var v = new SemVer.Version.parse ("2.1.8");
        if (v.major != 2 || v.minor != 1 || v.patch != 8) {
            Test.fail ();
        }
    });

    Test.add_func ("/semver/version/publish", () => {
        var v = new SemVer.Version (0, 1, 0);
        v.publish (VersionType.PATCH);
        if (v.neq (new SemVer.Version (0, 1, 1))) {
            Test.fail ();
        }

        v.publish (VersionType.MAJOR);
        if (v.neq (new SemVer.Version (1, 0, 0))) {
            Test.fail ();
        }

        v.publish (VersionType.PATCH);
        if (v.neq (new SemVer.Version (1, 0, 1))) {
            Test.fail ();
        }

        v.publish (VersionType.MINOR);
        if (v.neq (new SemVer.Version (1, 1, 0))) {
            Test.fail ();
        }
    });

    Test.add_func ("/semver/range/hyphen", () => {
        Range hyphen = new Range ("0.1.0 - 0.3.0");

        if (new SemVer.Version (0, 0, 1) in hyphen) {
            Test.fail ();
        }

        if (!(new SemVer.Version (0, 1, 0) in hyphen)) {
            Test.fail ();
        }

        if (!(new SemVer.Version (0, 2, 3) in hyphen)) {
            Test.fail ();
        }

        if (!(new SemVer.Version (0, 3, 0) in hyphen)) {
            Test.fail ();
        }

        if (new SemVer.Version (0, 4, 5) in hyphen) {
            Test.fail ();
        }
    });

    Test.add_func ("/semver/range/wildcard", () => {
        Range wc = new Range ("0.2.*");

        if (new SemVer.Version (0, 1, 0) in wc) {
            Test.fail ();
        }

        if (!(new SemVer.Version (0, 2, 0) in wc)) {
            Test.fail ();
        }

        if (!(new SemVer.Version (0, 2, 8) in wc)) {
            Test.fail ();
        }

        if (new SemVer.Version (0, 3, 0) in wc) {
            Test.fail ();
        }
    });

    Test.add_func ("/semver/range/carret", () => {
        Range carret = new Range ("^0.2.3");

        if (new SemVer.Version (0, 2, 0) in carret) {
            Test.fail ();
        }

        if (!(new SemVer.Version (0, 2, 3) in carret)) {
            Test.fail ();
        }

        if (!(new SemVer.Version (0, 2, 5) in carret)) {
            Test.fail ();
        }

        if (new SemVer.Version (0, 3, 0) in carret) {
            Test.fail ();
        }
    });

    Test.add_func ("/semver/range/tilde", () => {
        Range tilde = new Range ("~0.2");

        if (new SemVer.Version (0, 1, 9) in tilde) {
            Test.fail ();
        }

        if (!(new SemVer.Version (0, 2, 0) in tilde)) {
            Test.fail ();
        }

        if (!(new SemVer.Version (0, 2, 17) in tilde)) {
            Test.fail ();
        }

        if (new SemVer.Version (0, 3, 0) in tilde) {
            Test.fail ();
        }
    });

    Test.run ();

    return 0;
}
